﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    [SerializeField]
    /// <summary>
    /// Speed at wich laser moves.
    /// </summary>
    private float _speed = 7.0f;


    // Update is called once per frame
    void Update()
    {

        Movement();
    }

    private void Movement()
    {
        transform.Translate(Vector3.up * Time.deltaTime * _speed * 1);
        if (transform.position.y > 6.5f)
        {
            Destroy(gameObject);
            if (transform.parent)
            {
                Destroy(transform.parent.gameObject); //Destroys Empty Tripleshot  gameObject
            }
        }
    }
}
