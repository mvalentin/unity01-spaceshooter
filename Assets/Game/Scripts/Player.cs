﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Where all the functions related to a player are hosted.
/// </summary>
public class Player : MonoBehaviour
{
     /*************************************
     *                                    *
     *              References            *
     *                                    *
     **************************************/
       private UIManager _UIManager;
    /***************************************
    *                                      *
    *                Prefabs               *
    *                                      *
    ****************************************/

    [SerializeField]
    private GameObject _explosionPrefab;

    [SerializeField]
    /// <summary>
    /// Holds the laser prefab to be used
    /// </summary>
    private GameObject _laserPrefab;

    [SerializeField]
    /// <summary>
    /// Holds the tripple shot prefab to be used
    /// </summary>
    private GameObject _tripleShotPrefab;
    [SerializeField]
    private GameObject _shieldObj;

    /****************************************
    *                                       *                           
    *             Booleans                  *
    *                                       *
    *****************************************/
   /// <summary>
    /// Cooldown between shots.
    /// </summary>
    private bool _cooldown = false;

    /// <summary>
    /// Check if Triple Shot ability is enabled or not.
    /// </summary>
    private bool _tripleShotEnabled = false;

    /// <summary>
    /// Enables the boost for player
    /// </summary>
    private bool _speedBoostEnabled;
    /// <summary>
    /// Control Player's shield
    /// </summary>
    private bool _shieldsUp;

    /******************************************
    *                                         *
    *               Floats/Ints               *
    *                                         *
    *******************************************/


    [SerializeField]
    /// <summary>
    /// How much times must pass between shots.
    /// </summary>
    private float _limit = 0.2f;


    /// <summary>
    /// How much times pass between shots.
    /// </summary>
   private float _timer=0.0f;

 
    /// <summary>
    /// Holds the speed at which the lasers move
    /// </summary>
    private readonly float _defaultSpeed = 5.0f;
     [SerializeField]
     /// <summary>
     /// The sum of default speed + Bonus.
     /// </summary>
    private float _speed = 0f;

    [SerializeField]
    /// <summary>
    /// How much is added to the default player speed. (For future upgrade system if any)
    /// </summary>
    private float _speedBonus=0f;

    /// <summary>
    /// Player's Health
    /// </summary>
    private int _health = 3;


    /*************END OF VARIABLES********/
    void Start()
    {
        transform.position = new Vector3(0, 0, 0);
        _speed = _defaultSpeed;
        _UIManager=GameObject.Find("Canvas").GetComponent<UIManager>();
        if(_UIManager!=null)
        {_UIManager.UpdateLives(_health);
        }
     
    }

    void Update()
    {

        Movement();
        Fire();
        CheckHealth();
        //Debug.Log(_speed);


    }
    /**************************************
    *                                     *
    *          Player Interactor          *
    *                                     * 
    ***************************************/
    /// <summary>
    /// Controls the movement of the player such as speed and boundaries.
    /// </summary>
    private void Movement()
    {
        _speed = _defaultSpeed + _speedBonus;
     

        float horizontalInput = Input.GetAxis("Horizontal");
        float veritcalInput = Input.GetAxis("Vertical");

        transform.Translate(Vector3.right * Time.deltaTime * _speed * horizontalInput);
        transform.Translate(Vector3.up * Time.deltaTime * _speed * veritcalInput);

        if (transform.position.y > 0)
        {
            transform.position = new Vector3(transform.position.x, 0, 0);
        }
        else if (transform.position.y < -4.2f)
        {
            transform.position = new Vector3(transform.position.x, -4.2f, 0);
        }

        if (transform.position.x > 9.5f)
        {
            transform.position = new Vector3(-9.5f, transform.position.y, 0);
        }
        else if (transform.position.x < -9.5f)
        {
            transform.position = new Vector3(9.5f, transform.position.y, 0);
        }
    }

    /// <summary>
    /// Holds  control of attacking abilities
    /// </summary>
    private void Fire()
    {
        if (!_cooldown)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (_tripleShotEnabled)
                {
                    Instantiate(_tripleShotPrefab, transform.position + new Vector3(0, 0f, 0), Quaternion.identity);
                }
                else {
                Instantiate(_laserPrefab, transform.position + new Vector3(0, 0.93f, 0), Quaternion.identity); 
                }

                _cooldown = true;
            }

        }
        else
        {
            if (_timer <= _limit && _cooldown)
            {
                _timer += Time.deltaTime;
               
            }
            else {
                _cooldown = false;
                _timer = 0f;
            }
        }

    }

    /// <summary>
    /// Checks the health of player if is == 0 it deletes the player's gameObject.
    /// </summary>
    private void CheckHealth()
    {
        if (_health<=0)
        {
            Destroy(gameObject);
            Instantiate(_explosionPrefab, transform.position, Quaternion.identity);
      }
    }
    /// <summary>
    /// Used for external script take health out of player.
    /// </summary>
    public void TakeDamage()
    { if (_shieldsUp == false)
        {
            _health--;
             _UIManager.UpdateLives(_health);

        }
        else {
            var shield =this.transform.Find("Shields");
            Debug.Log(shield);
            Destroy(shield.gameObject);
        ;
    
         _shieldsUp = false;
        }


        Debug.Log("Health: " + _health);
    }


    /**************************************
    *                                     *
    *         PowerUps Enablers           *
    *                                     *
    ***************************************/
    /// <summary>
    /// Enables the Triple Shot Powerup
    /// </summary>
    public void EnableTripleShot() {
        _tripleShotEnabled = true;
        StartCoroutine(TripleShotPowerDownRoutine());
     }

    /// <summary>
    /// Enables the Boost Powerup
    /// </summary>
    public void EnableBoost()
    {
        _speedBoostEnabled = true;
        StartCoroutine(BoostPowerDownRoutine());
        _speedBonus = _defaultSpeed*0.5f;
    }
    public void EnableShield()
    {
        _shieldsUp = true;
        var shield= Instantiate(_shieldObj, transform.position + new Vector3(0, 0f, 0), Quaternion.identity);
        shield.transform.parent=gameObject.transform;
        shield.name="Shields";

          }

    /*****************************************
    *                                        *
    *            Coroutines                  *
    *                                        *
    ******************************************/
    /// <summary>
    /// Disables the boost after a period of time
    /// </summary>
    /// <returns>triplShotEnabled=false</returns>
    private IEnumerator BoostPowerDownRoutine()
    {
        yield return new WaitForSeconds(5.0f);
        _speedBoostEnabled = false;
        _speedBonus = 0f;

    }
     /// <summary>
    /// Disables the Triple Shot after a period of time
    /// </summary>
    /// <returns>triplShotEnabled=false</returns>
   private IEnumerator TripleShotPowerDownRoutine()
    {
        yield return new WaitForSeconds(5.0f);
        _tripleShotEnabled = false;
 
    }
   


}

