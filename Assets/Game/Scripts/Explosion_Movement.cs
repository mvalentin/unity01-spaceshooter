﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion_Movement : MonoBehaviour
{
    private float _speed = 2.5f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.down * Time.deltaTime * _speed * 1);

        if (transform.position.y < -8f)
        {
            Destroy(gameObject);
        }

    }

   
}
