﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
[SerializeField]
    private GameObject _explosionPrefab;
    private float _speed = 2.5f;
    private float _health = 100f;
    private UIManager _UIManager;


    // Start is called before the first frame update
    void Start()
    {
        _UIManager=GameObject.Find("Canvas").GetComponent<UIManager>();
    }

    // Update is called once per frame
    void Update()
    {

        Movement();

    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {

            Player player = other.GetComponent<Player>();
            player.TakeDamage();
            CheckHealth();

        }

        else if(other.CompareTag("Laser"))
        {

            CheckHealth();

            if (other.transform.parent!=null)
            {
                Destroy(other.transform.parent.gameObject);
            }
            Destroy(other.gameObject);
        }


    }
    /// <summary>
    /// Controls the random Spawn and the going down
    /// </summary>
    private void Movement()
    {

        transform.Translate(Vector3.down * Time.deltaTime * _speed*1);

        if (transform.position.x > 9.5f)
        {
            transform.position = new Vector3(-9.5f, transform.position.y, 0);
        }

        else if (transform.position.x < -9.5f)
        {
            transform.position = new Vector3(9.5f, transform.position.y, 0);
        }

        if (transform.position.y < -8f)
        {
            transform.position = new Vector3(Random.Range(-9f,9f), 9f, 0);
        }

    }
    private void CheckHealth()
    {
        Destroy(gameObject);
        Instantiate(_explosionPrefab, transform.position, Quaternion.identity);
        _UIManager.UpdateScore();
        
    }

}
