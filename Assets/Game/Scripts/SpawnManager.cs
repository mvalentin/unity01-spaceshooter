﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
[SerializeField]

 /// <summary>
    /// Holds the enemyPrefab
    /// </summary>
private GameObject _enemyShipPrefab; 
[SerializeField]
/// <summary>
    /// Holds the Powerups
    /// </summary>
private GameObject[] powerups;

private void Start()
{
 StartCoroutine(SpawnEnemy());
 StartCoroutine(SpawnPowerUp());
}

private void Update()
{

}
    
private IEnumerator SpawnEnemy()
    {

while (true)
{
     yield return new WaitForSeconds(5.0f);
           Instantiate(_enemyShipPrefab,new Vector3(Random.Range(-7f,7f), 9f, 0), Quaternion.identity);
         
    }
}

private IEnumerator SpawnPowerUp(){
    while (true)
    {
         int randomPowerup=Random.Range(0,3);
    Instantiate(powerups[randomPowerup],new Vector3 (Random.Range(-7f,7f),7f , 0f),Quaternion.identity);
 yield return new WaitForSeconds(Random.Range(5f,10f));
    }
   
}
};