﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    [SerializeField]
    private float _speed = 3.0f;
    [SerializeField]
    private int _powerUpID;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
    }
    /// <summary>
    /// Used for detection of Poweup collition and type
    /// </summary>
    /// <param name="other">The Collider of the other object.</param>
    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.CompareTag("Player"))
        {
            Debug.Log("Collision with " + other.name);
            Player player = other.GetComponent<Player>();
            if (player!=null)
            {
                if (_powerUpID==0)
                { 
                    player.EnableTripleShot(); 
                }
                else if (_powerUpID==1)
                {
                    player.EnableBoost();
                }
                else if (_powerUpID==2)
                {
                 
                    player.EnableShield();
                }

            }
           
            Destroy(gameObject);
           
        }
    
    }
    /// <summary>
    /// Controls the movement of the powerups such as speed and boundaries.
    /// </summary>
    private void Movement()
    {
        transform.Translate(Vector3.down * _speed* Time.deltaTime);

        if (transform.position.y < -5.5)
        {
            Destroy(gameObject);
        }
    }
   
}
