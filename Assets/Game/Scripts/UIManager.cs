﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; // For Image type

public class UIManager : MonoBehaviour
{
    public Sprite[] lives;
    public Image liveImage;
    public Text scoreText;
    private int score=0;
    public void UpdateLives(int currentLives)
    {
        liveImage.sprite = lives[currentLives];  
    }
    public void UpdateScore(){
        score+=10;
        scoreText.text="Score: " + score;


    }
  
}
